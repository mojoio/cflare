/**
 * autocreated commitinfo by @pushrocks/commitinfo
 */
export const commitinfo = {
  name: '@apiclient.xyz/cloudflare',
  version: '6.0.4',
  description: 'easy cloudflare management'
}
